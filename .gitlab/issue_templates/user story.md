As a ‹role›, I'd like to ‹feature short description› [ , in order to ‹value it adds›. ]

## Conditions of satisfaction

- [ ] Should ‹testable condition that should be satisfied›
- [ ] Should ‹testable condition that should be satisfied›
- [ ] …

## Delete following sections

### Roles

* _Anonymous User_ of the Testlink System
* _Customer_ of the Testlink System
* _Staff member_ of Testlink
* _Administrator of the Testlink System
* _Programmer_ of the Testlink System

### A good user story should be (I-N-V-E-S-T principle)

* _Independent_ (from other user stories, allowing to realize them in any order);
* _Negotiable_ (omit details that would freeze the story);
* _Valuable_ (implementation delivers an increment of functionality, observable by and useful to users);
* _Estimatable_ (developers should be able to estimate its size relative to other stories);
* _Sizable_ (implementation fits in one iteration – if it needs many to complete, it is an EPIC);
* _Testable_ (user must be able to check the conditions of satisfaction).